﻿using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public static class Extras
{
    public static bool CompareVector2D(Vector2 vectorA, Vector2 vectorB, float tolerance = .001f)
    {
        if(Mathf.Abs(vectorB.x - vectorA.x) < tolerance && Mathf.Abs(vectorB.y - vectorA.y) < tolerance)
            return true;
        else
            return false;
    }

    public static bool CompareRotation2D(float rotA, float rotB, float tolerance = .001f)
    {
        if(Mathf.Abs(Mathf.DeltaAngle(rotA, rotB)) < tolerance)
            return true;
        else
            return false;
    }

    public static void SwapInt(ref int a, ref int b)
    {
        int temp = a;
        a = b;
        b = temp;
    }

    public static void SwapFloat(ref float a, ref float b)
    {
        float temp = a;
        a = b;
        b = temp;
    }

    public static Vector2 ClampVector2(Vector2 vector, Vector2 limit1, Vector2 limit2)
    {
        vector.x = Mathf.Clamp(vector.x, limit1.x, limit2.x);
        vector.y = Mathf.Clamp(vector.y, limit1.y, limit2.y);

        return vector;
    }


    public static float Remap(float f, float old1, float old2, float new1, float new2, bool clamp = false)
    {
        if(clamp)
        {
            if(old1 > old2)
                SwapFloat(ref old1, ref old2);

            Mathf.Clamp(f, old1, old2);
        }

        return new1 + (new2 - new1) * (f - old1) / (old2 - old1);
    }

    public static char GetRandomChar()
    {
        //string chars = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&";
        string chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        System.Random rand = new System.Random();
        int num = rand.Next(0, chars.Length - 1);
        return chars[num];
    }

    public static string GetRandomString(int length)
    {
        string chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string toReturn = "";
        System.Random rand = new System.Random();
        for(int i = 0; i < length; i++)
        {
            int num = rand.Next(0, chars.Length - 1);
            toReturn += chars[num];
        }
        return toReturn;
    }

    public static Vector2 GetClosestCornerVecOne(Vector2 screenPosition)
    {
        float x = 1;
        float y = 1;

        if(screenPosition.x > Screen.width / 2)
            x = 1;
        else
            x = -1;

        if(screenPosition.y > Screen.height / 2)
            y = 1;
        else
            y = -1;

        return new Vector2(x, y);
    }

    public static bool GetProbability(float value)
    {
        return (UnityEngine.Random.value < value);
    }

    public static bool IsPointerOverUiObject()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}