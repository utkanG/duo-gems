﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell
{
    // position of a cell is only set when the grid is created
    public Vector2 position;
    public Item Item;

    public Cell(Vector2 pos)
    {
        position = pos;
    }

    public void AssignItem(Item newDot)
    {
        Item = newDot;
    }
}