﻿using System.Text;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods
{
    public static int ParseFast(this string s)
    {
        int r = 0;
        for(var i = 0; i < s.Length; i++)
        {
            char letter = s[i];
            r += System.Convert.ToInt32(letter);
        }
        return r;
    }

    public static int ASCIISequenceToInt(this string s)
    {
        string intAsString = "";
        for(var i = 0; i < s.Length; i++)
        {
            char letter = s[i];
            string toAdd = System.Convert.ToInt32(letter).ToString();

            int diff = 3 - toAdd.Length;
            int currentExtender = 9;
            for(int a = 0; a < diff; a++)
            {
                intAsString += currentExtender.ToString();
                currentExtender--;
            }
            intAsString += toAdd;
        }

        int r = intAsString.ParseFast(); // just in case parse fails
        if(!int.TryParse(intAsString, out r))
        {
            Debug.LogError("Did you enter a seed with more than 3 letters. How dare you? ");
        }

        return r;
    }

    public static Vector2 Clamp(this Vector2 v, Vector2 limit1, Vector2 limit2)
    {
        v.x = Mathf.Clamp(v.x, limit1.x, limit2.x);
        v.y = Mathf.Clamp(v.y, limit1.y, limit2.y);

        return v;
    }

    public static bool CompareVector2D(this Vector2 vectorA, Vector2 vectorB, float tolerance = .001f)
    {
        if(Mathf.Abs(vectorB.x - vectorA.x) < tolerance && Mathf.Abs(vectorB.y - vectorA.y) < tolerance)
            return true;
        else
            return false;
    }

    public static float Remap(this float f, float old1, float old2, float new1, float new2, bool clamp = false)
    {
        if(clamp)
        {
            if(old1 > old2)
                Extras.SwapFloat(ref old1, ref old2);

            Mathf.Clamp(f, old1, old2);
        }

        return new1 + (new2 - new1) * (f - old1) / (old2 - old1);
    }

    public static float GetRotation2D(this Transform transform)
    {
        return transform.eulerAngles.z;
    }

    public static void SetRotation2D(this Transform transform, float rotation)
    {
        Vector3 r = transform.eulerAngles;
        r.z = rotation;
        transform.eulerAngles = r;
    }

    public static float LookRotation2D(this Transform t, Vector2 direction)
    {
        return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
    }

    public static float LookRotation2D(this Transform t, Vector3 targetPosition)
    {
        Vector2 v = targetPosition - t.position;
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90;
    }

    public static float LookRotation2D(this Transform t, Transform target)
    {
        if(target == null)
            return t.rotation.eulerAngles.z;

        Vector2 v = target.transform.position - t.position;
        return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90;
    }

    public static void RotateTowards2D(this Rigidbody2D rb, float targetRotation, float maxDegreesDelta)
    {
        rb.MoveRotation(Mathf.MoveTowardsAngle(rb.transform.eulerAngles.z, targetRotation, maxDegreesDelta));
    }

    public static void RotateTowards2D(this Transform t, float targetRotation, float maxDegreesDelta)
    {
        t.SetRotation2D(Mathf.MoveTowardsAngle(t.eulerAngles.z, targetRotation, maxDegreesDelta));
    }

    public static void AddSpacesToSentence(this string text, bool preserveAcronyms)
    {
        if(string.IsNullOrWhiteSpace(text))
            return;
        StringBuilder newText = new StringBuilder(text.Length * 2);
        newText.Append(text[0]);
        for(int i = 1; i < text.Length; i++)
        {
            if(char.IsUpper(text[i]))
                if((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
                    (preserveAcronyms && char.IsUpper(text[i - 1]) &&
                        i < text.Length - 1 && !char.IsUpper(text[i + 1])))
                    newText.Append(' ');
            newText.Append(text[i]);
        }
        text = newText.ToString();
    }

    public static void DestroyAllChildren(this Transform t)
    {
        int childCount = t.childCount;
        for(int i = 0; i < childCount; i++)
        {
            GameObject child = t.GetChild(i).gameObject;
            GameObject.Destroy(child);
        }
    }
} 