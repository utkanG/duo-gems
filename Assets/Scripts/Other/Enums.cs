﻿
public enum GameState
{

}

public enum UltimateType
{
    // make the row and the column into one, wtf
    RowRocket,
    ColumnRocket,
    Bomb,
    Disco,
    None, // exists as a fallback
}

public enum ItemType
{
    Basic,
    Ultimate
}

public enum Direction
{
    Horizontal,
    Vertical
}