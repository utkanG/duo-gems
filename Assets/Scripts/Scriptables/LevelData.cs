﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Level", menuName = "Level Data", order = 2)]
public class LevelData : ScriptableObject
{
    public Vector2Int Size;

    public List<Item> LevelItems;
}
