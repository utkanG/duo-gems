﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item Data", order = 1)]
public class ItemData : ScriptableObject
{
    public int ID;
    public Sprite Sprite;
}
