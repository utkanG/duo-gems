﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

// Grid generation and cell holder
public class Board : MonoBehaviour
{
    [Header("Properties")]
    public float gravity = 5;
    public float maxAccelerationDuration = 1;
    public AnimationCurve bounceEase;

    public List<Item> LevelItems;

    [Header("References")]
    [SerializeField]
    private VFXControl vfxControl;
    [SerializeField]
    private Sprite[] ultiMarkerSprites;
    [SerializeField]
    private UltimateItem[] ultimateItems;
    [SerializeField]
    private Sprite[] discoSprites;

    private Cell[,] cells;
    private List<Item> allItems = new List<Item>();

    private Vector2Int size;

    // two lists are needed and be modified at the same time to prevent unneccessary null checks and a rare frame bug
    private HashSet<Item> itemsToMove = new HashSet<Item>();
    private HashSet<Item> movedItems = new HashSet<Item>();
    
    // for coroutines, when a trigger chain is in progress input is also disabled
    private HashSet<Item> toRemove = new HashSet<Item>();
    private int[] spawnCountAtColumn;

    private int onGoingRoutineCount;
    private int onWaitDiscoCount;

    [HideInInspector]
    public UnityEvent GridUpdated;

    // this should be combined with or transferred to the planned, column based item movement stoppers. (Those boxes from Toon Blast etc.)
    private float haltDuration;

    private GameControl gameControl;

    private void Awake()
    {
        gameControl = FindObjectOfType<GameControl>();
    }

    private void Start()
    {
        onGoingRoutineCount = 0;
    }

    public void GenerateGrid(LevelData lvlData)
    {
        StopAllCoroutines();

        cells = new Cell[lvlData.Size.x, lvlData.Size.y];
        ClearBoard();

        size = lvlData.Size;
        spawnCountAtColumn = new int[lvlData.Size.x];

        LevelItems = new List<Item>(lvlData.LevelItems);

        // create cells according to level and place dots
        int cellIndex = 0;
        for(int x = 0; x < lvlData.Size.x; x++)
        {
            for(int y = 0; y < lvlData.Size.y; y++)
            {
                Cell newCell = new Cell(new Vector2(x,y));
                cells[x, y] = newCell;

                cellIndex++;

                Item newItem = Instantiate(LevelItems[Random.Range(0, LevelItems.Count)], transform).GetComponent<Item>();
                newItem.transform.position = newCell.position + (size.y * 1.5f + y * 1.1f + 1) * Vector2.up;
                newItem.RemainingDistance = size.y * 1.5f + y * 1.1f + 1;
                allItems.Add(newItem);
                itemsToMove.Add(newItem);
                newItem.CellPos = newCell.position;

                //newItem.transform.localScale = Vector3.zero;
                //newItem.transform.DOScale(1, .4f).SetEase(Ease.OutElastic);

                newCell.AssignItem(newItem);
            }
        }
    }

    private void ClearBoard()
    {
        toRemove.Clear();
        itemsToMove.Clear();
        movedItems.Clear();

        foreach(Item item in allItems)
        {
            Destroy(item.gameObject);
        }
        allItems.Clear();
    }

    public Item GetItem(Vector2 position)
    {
        position = position.Clamp(Vector2.zero, size - Vector2.one);

        return cells[Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y)].Item;
    }

    public Item GetItem(Cell cell)
    {
        return cell.Item;
    }

    public Cell GetCell(Vector2 position)
    {
        position = position.Clamp(Vector2.zero, size - Vector2.one);

        return cells[Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y)];
    }

    public bool IsInsideGrid(Vector2 position)
    {
        return position.x < (size.x - 1) + .4f && position.y < (size.y - 1) + .4f && position.x > -.4f && position.y > -.4f;
    }

    public int GetUltiConnections(Cell cell, HashSet<Item> results, bool includeDiagonal = false)
    {
        if(!cell.Item.DoneMoving || !(cell.Item is UltimateItem))
            return results.Count;

        results.Add(cell.Item);

        Vector2 origin = cell.position;
        Cell toBeChecked = cell;

        for(int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                if(!includeDiagonal)
                {
                    if(Mathf.Abs(x) == Mathf.Abs(y))
                        continue;
                }

                toBeChecked = GetCell(origin + new Vector2(x, y));
                if(toBeChecked != null)
                {
                    if(!results.Contains(toBeChecked.Item))
                        if(toBeChecked.Item is UltimateItem)
                        {
                            GetUltiConnections(toBeChecked, results, includeDiagonal);
                        }
                }
            }
        }

        return results.Count;
    }

    public int GetBasicConnections(Cell cell, HashSet<Item> results, bool includeDiagonal = false)
    {
        if(!cell.Item.DoneMoving || !(cell.Item is BasicItem))
            return results.Count;

        results.Add(cell.Item);

        Vector2 origin = cell.position;
        Cell toBeChecked = cell;

        for(int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                if(!includeDiagonal)
                {
                    if(Mathf.Abs(x) == Mathf.Abs(y))
                        continue;
                }

                toBeChecked = GetCell(origin + new Vector2(x, y));
                if(toBeChecked != null)
                {
                    if(!results.Contains(toBeChecked.Item))
                        if(toBeChecked.Item.ID == cell.Item.ID)
                        {
                            GetBasicConnections(toBeChecked, results, includeDiagonal);
                        }
                }
            }
        }

        return results.Count;
    }

    private void RemoveItems(ICollection<Item> toBeRemoved)
    {
        int[] totalRemovalInColumn = new int[size.x];
        // check cells, if a dot is removed add movement to column
        for(int x = 0; x < size.x; x++)
        {
            for(int y = 0; y < size.y; y++)
            {
                Item pointedItem = GetItem(cells[x, y]);
                if(toBeRemoved.Contains(pointedItem))
                {
                    totalRemovalInColumn[x]++;
                }
                else // means that the dot isnt removed and if there is a removal in column, we add movement to it
                {
                    pointedItem.RemainingDistance += totalRemovalInColumn[x];

                    if(pointedItem.RemainingDistance > 0)
                    {
                        // assign the cell that should receive it
                        Cell cell = GetCell(new Vector2(x, y - totalRemovalInColumn[x]));
                        cell.Item = pointedItem;
                        pointedItem.CellPos = cell.position;
                        //Debug.DrawLine(cell.position, cell.position + Vector2.up * .5f, Color.red, 1f);
                        pointedItem.DoneMoving = false;
                        pointedItem.transform.DOComplete();

                        if(itemsToMove.Add(pointedItem))
                            movedItems.Remove(pointedItem);
                    }
                }
            }

            // dots that will spawn
            for(int i = 0; i < totalRemovalInColumn[x]; i++)
            {
                Item newItem = Instantiate(LevelItems[Random.Range(0, LevelItems.Count)], transform).GetComponent<Item>();

                // assign the cell that will receive it ("size.y - 1" since the bottom part is 0 and no cell exists at size.y)
                Cell cell = GetCell(new Vector2(x, size.y - totalRemovalInColumn[x] + i));
                cell.Item = newItem;
                newItem.CellPos = cell.position;

                spawnCountAtColumn[x]++;

                float extraHeight = spawnCountAtColumn[x];
                newItem.transform.position = new Vector2(x, i + size.y * 1.25f + extraHeight);
                newItem.DoneMoving = false;
                newItem.RemainingDistance = totalRemovalInColumn[x] + size.y / 4f + extraHeight;

                vfxControl.PlaySelectEffect(newItem.transform.position, newItem.FXColor);
                newItem.transform.localScale = Vector3.zero;
                newItem.transform.DOScale(1, .4f).SetEase(Ease.OutElastic);

                allItems.Add(newItem);
                itemsToMove.Add(newItem);
                movedItems.Remove(newItem);
            }
        }

        // absoulute destroy or pooling stuff goes here (these are triggered items only)
        foreach(Item item in toBeRemoved)
        {
            allItems.Remove(item);

            // something is off
            if(item)
                Destroy(item.gameObject);
        }

        UpdateMergeMarkers();

        toRemove.Clear();
    }

    public void BlastBasicItems(HashSet<Item> blastedItems)
    {
        if(blastedItems.Count == 1)
        {
            foreach(Item item in blastedItems)
            {
                item.transform.DOPunchScale(Vector3.one * .2f, .1f).OnComplete(() => { item.transform.localScale = Vector3.one; });
            }
            return;
        }
        else
        {
            foreach(Item item in blastedItems)
            {
                TriggerItem(item);
            }
            RemoveItems(blastedItems);
        }
    }

    // look at all those chicke... coroutines.............
    // try to put this part into another class and make them take the board as reference maybe?
    #region AllRoutines

    // ----- ULTI ITEMS ------

    private IEnumerator RocketRoutine(UltimateItem ultiItem, Direction dir, bool forceRemove = false)
    {
        onGoingRoutineCount++;

        Cell pointedCell = GetCell(ultiItem.CellPos);
        Vector3 originPos = pointedCell.position;
        Vector2 pointedPos = originPos;
        WaitForSeconds delayBetweenItems = new WaitForSeconds(.08f);

        toRemove.Add(ultiItem);

        int maxSize = (dir == Direction.Horizontal) ? size.x : size.y;
        int startAngle = (dir == Direction.Horizontal) ? 0 : 90;

        vfxControl.PlayRocketEffect(ultiItem.CellPos, startAngle);

        // disgusting 1 representing 2 sides
        for(int x = 1; x <= size.x; x++)
        {
            bool delay = false;

            for(int i = 0; i < 2; i++)
            {
                pointedPos = originPos + (Quaternion.AngleAxis(180 * i + startAngle, Vector3.forward) * Vector3.right) * x;

                if(IsInsideGrid(pointedPos))
                {
                    pointedCell = GetCell(pointedPos);

                    Item item = pointedCell.Item;
                    {
                        delay = true;
                        if(toRemove.Add(item))
                            TriggerItem(item, forceRemove);
                    }
                }
            }

            // break lazım bi yerlerde
            if(delay)
                yield return delayBetweenItems;
            else
                continue;
        }

        if(forceRemove)
            RemoveItems(toRemove);

        onGoingRoutineCount--;
    }

    private IEnumerator BombRoutine(UltimateItem ultiItem, bool forceRemove = false)
    {
        onGoingRoutineCount++;

        yield return new WaitForFixedUpdate();

        Cell pointedCell = GetCell(ultiItem.CellPos);
        Vector2 originPos = pointedCell.position;
        Vector2 pointedPos = originPos;

        toRemove.Add(ultiItem);

        for(int x = -1; x <= 1; x++)
        {
            for(int y = -1; y <= 1; y++)
            {
                pointedPos = originPos + new Vector2(x, y);

                if(IsInsideGrid(pointedPos))
                {
                    pointedCell = GetCell(pointedPos);

                    Item item = pointedCell.Item;
                    {
                        if(toRemove.Add(item))
                            TriggerItem(item, forceRemove);
                    }
                }
            }
        }

        if(forceRemove)
            RemoveItems(toRemove);

        onGoingRoutineCount--;
    }

    private IEnumerator DiscoRoutine(DiscoItem discoItem)
    {
        onGoingRoutineCount++;
        onWaitDiscoCount++;

        // trigger anim
        HashSet<Item> markedItems = new HashSet<Item>();

        while(onGoingRoutineCount > onWaitDiscoCount || itemsToMove.Count > 0)
        {
            yield return new WaitForSeconds(.25f);
        }

        onWaitDiscoCount--;

        markedItems = FindAllItemsOfID(discoItem.markedID);
        foreach(Item item in markedItems)
        {
            if(item.RemainingDistance < .25f)
                if(toRemove.Add(item))
                {
                    // send connection fx here
                    vfxControl.PlayLandEffect(item.CellPos);
                    item.transform.DOPunchScale(Vector3.one / 4, .5f);
                    item.MoveToFront(1);
                    yield return new WaitForSeconds(Random.Range(.025f,.1f));
                }
        }

        vfxControl.PlayUltiEffect(discoItem.CellPos);
        yield return new WaitForSeconds(.75f);

        foreach(Item item in markedItems)
        {
            TriggerItem(item);
        }

        if(onWaitDiscoCount == 0)
        {
            toRemove.Add(discoItem);
            RemoveItems(toRemove);
        }

        onGoingRoutineCount--;
    }

    // ----- MERGE METHODS ------

    // hmmmmm
    public IEnumerator MergeBasicItems(HashSet<Item> toBeMerged, Cell mergeCell)
    {
        toBeMerged.Remove(mergeCell.Item);

        HashSet<Item> toBeMergedCopy = new HashSet<Item>(toBeMerged); // copied to prevent the clearing of the list, since it is delayed

        foreach(Item item in toBeMergedCopy)
        {
            vfxControl.PlayLandEffect(item.CellPos);
            item.Merge(mergeCell.position);
        }

        yield return new WaitForSeconds(.35f);

        vfxControl.PlayRemovalEffect(mergeCell.position, Color.magenta);

        UltimateType ultimateType = ChooseType(toBeMergedCopy.Count + 1); // +1 for the removed
        UltimateItem newUltiItem = Instantiate(ultimateItems[(int) ultimateType], transform).GetComponent<UltimateItem>();
        if(ultimateType == UltimateType.Disco)
        {
            DiscoItem discoItem = newUltiItem as DiscoItem;
            discoItem.markedID = mergeCell.Item.ID;
            discoItem.UpdateSprite(discoSprites[discoItem.markedID - 1]);
        }

        Destroy(mergeCell.Item.gameObject);
        allItems.Remove(mergeCell.Item);

        allItems.Add(newUltiItem);
        mergeCell.Item = newUltiItem;
        newUltiItem.CellPos = mergeCell.position;
        newUltiItem.CompleteMove();

        RemoveItems(toBeMergedCopy);
        gameControl.InputEnabled = true;
    }

    public IEnumerator MergeUltiItems(HashSet<Item> toBeMerged, Cell mergeCell)
    {
        HashSet<Item> toBeMergedCopy = new HashSet<Item>(toBeMerged); // copied to prevent the clearing of the list, since it is delayed

        WaitForSeconds whileDelay = new WaitForSeconds(.1f);

        foreach(Item item in toBeMergedCopy)
        {
            vfxControl.PlayLandEffect(item.CellPos);
            item.Merge(mergeCell.position);
        }

        yield return new WaitForSeconds(.35f);

        // remove items without triggering them
        toRemove.UnionWith(toBeMergedCopy);
        foreach(Item item in toBeMergedCopy)
        {
            item.gameObject.SetActive(false);
        }

        vfxControl.PlayRemovalEffect(mergeCell.position, Color.magenta);

        // kinda weird
        int[] finalValues = new int[2];
        finalValues[0] = 0;
        finalValues[1] = 0;

        int markedID = 0; // for a disco case, this got so weird

        foreach(UltimateItem item in toBeMergedCopy)
        {
            int minValue = Mathf.Min(finalValues[0], finalValues[1]);
            if(minValue < item.MergeValue)
            {
                if(minValue == finalValues[0])
                    finalValues[0] = item.MergeValue;
                else
                    finalValues[1] = item.MergeValue;
            }
            else
            {
                continue;
            }

            DiscoItem possibleDiscoItem = item as DiscoItem;

            if(possibleDiscoItem)
                markedID = possibleDiscoItem.markedID;
        }

        // animation plays here
        vfxControl.PlayUltiEffect(mergeCell.position);
        yield return new WaitForSeconds(.5f);

        // passing in a markedID JUST for a disco merge with other items, this is too spesific
        HandleUltiMerge(finalValues, mergeCell.position , markedID);

        while(onGoingRoutineCount > 0)
        {
            yield return whileDelay;
        }

        OnRoutinesEnded();
    }

    // ----- MERGE ULTIS ------
    // these ultis will probably wait for all items to fall and get in position. if otherwise, make them wait like the disco routine

    private IEnumerator DualRocketRoutine(Vector2 cellPos)
    {
        onGoingRoutineCount++;

        Cell pointedCell = GetCell(cellPos);
        Vector3 originPos = pointedCell.position;
        Vector2 pointedPos = originPos;
        WaitForSeconds delayBetweenItems = new WaitForSeconds(.08f);

        vfxControl.PlayRocketEffect(cellPos, 0);
        vfxControl.PlayRocketEffect(cellPos, 90);

        int maxSize = Mathf.Max(size.x, size.y);
        // disgusting 1 representing 2 sides
        for(int s = 1; s <= maxSize; s++)
        {
            bool delay = false;

            for(int i = 0; i < 4; i++)
            {
                pointedPos = originPos + (Quaternion.AngleAxis(90 * i, Vector3.forward) * Vector3.right) * s;

                if(IsInsideGrid(pointedPos))
                {
                    pointedCell = GetCell(pointedPos);

                    Item item = pointedCell.Item;
                    {
                        delay = true;
                        if(toRemove.Add(item))
                            TriggerItem(item);
                    }
                }
            }

            // break lazım bi yerlerde
            if(delay)
                yield return delayBetweenItems;
            else
                continue;
        }

        onGoingRoutineCount--;
    }

    private IEnumerator BombRocketRoutine(Vector2 cellPos)
    {
        onGoingRoutineCount++;

        Cell pointedCell = GetCell(cellPos);
        Vector3 originPos = pointedCell.position;
        Vector2 pointedPos = originPos;
        WaitForSeconds delayBetweenItems = new WaitForSeconds(.08f);

        for(int x = -1; x <= 1; x++)
        {
            vfxControl.PlayRocketEffect(originPos + new Vector3(x, 0), 90);
        }

        for(int y = -1; y <= 1; y++)
        {
            vfxControl.PlayRocketEffect(originPos + new Vector3(0 ,y), 0);
        }

        int maxSize = Mathf.Max(size.x, size.y);
        for(int i = 1; i <= maxSize; i++)
        {
            bool delay = false;

            for(int x = -1 * i; x <= 1 * i; x++)
            {
                for(int y = -1 * i; y <= 1 * i; y++)
                {
                    if(Mathf.Abs(x) > 1 && Mathf.Abs(y) > 1)
                    {
                        continue;
                    }

                    pointedPos = originPos + new Vector3(x, y);

                    // may be smarter with more math, lots of IsInsideGrid checks right now
                    if(IsInsideGrid(pointedPos))
                    {
                        pointedCell = GetCell(pointedPos);

                        Item item = pointedCell.Item;
                        {
                            delay = true;
                            if(toRemove.Add(item))
                                TriggerItem(item);
                        }
                    }
                }
            }

            // break lazım bi yerlerde
            if(delay)
                yield return delayBetweenItems;
            else
                continue;
        }

        onGoingRoutineCount--;
    }

    private IEnumerator LargeBombRoutine(Vector2 cellPos, int range)
    {
        onGoingRoutineCount++;

        Cell pointedCell = GetCell(cellPos);
        Vector3 originPos = pointedCell.position;
        Vector2 pointedPos = originPos;
        WaitForSeconds shockwaveDelay = new WaitForSeconds(.05f);

        // disgusting 1 representing 2 sides
        for(int i = 1; i <= range; i++)
        {
            bool delay = false;

            for(int x = -1 * i; x <= 1 * i; x++)
            {
                for(int y = -1 * i; y <= 1 * i; y++)
                {
                    if(Mathf.Abs(x) != i && Mathf.Abs(y) != i) continue; // only x and y create interesting results

                    pointedPos = originPos + new Vector3(x, y);

                    // may be smarter with more math, lots of IsInsideGrid checks right now
                    if(IsInsideGrid(pointedPos))
                    {
                        pointedCell = GetCell(pointedPos);

                        Item item = pointedCell.Item;
                    if(item.RemainingDistance < .25f)
                        {
                            delay = true;
                            if(toRemove.Add(item))
                                TriggerItem(item);
                        }
                    }
                }
            }

            // break lazım bi yerlerde
            if(delay)
                yield return shockwaveDelay;
            else
                continue;
        }

        onGoingRoutineCount--;
    }

    private IEnumerator AdvancedDiscoRoutine(Vector2 cellPos, int markedID, int mergeValue)
    {
        onGoingRoutineCount++;
        onWaitDiscoCount++;

        // trigger anim
        HashSet<Item> markedItems = new HashSet<Item>();
        List<UltimateItem> ultiItems = new List<UltimateItem>();

        while(onGoingRoutineCount > onWaitDiscoCount)
        {
            yield return new WaitForSeconds(.25f);
        }

        onWaitDiscoCount--;
        // disco animation should start playing here

        markedItems = FindAllItemsOfID(markedID);
        foreach(Item item in markedItems)
        {
            if(item.DoneMoving)
            {
                // send connection fx here
                vfxControl.PlayLandEffect(item.CellPos);
                vfxControl.PlayRemovalEffect(item.CellPos, item.FXColor);

                item.transform.DOPunchScale(Vector3.one / 4, .5f);
                item.MoveToFront(1);

                yield return new WaitForSeconds(Random.Range(0f, .1f));

                // randomize rockets, shouldnt be here if rockets are combined, mmmmmmmmmmmmmmmm no
                UltimateType transformType = UltimateType.Bomb;
                if(mergeValue == 1)
                    transformType = (UltimateType) Random.Range(0, 2);

                UltimateItem newUltiItem = Instantiate(ultimateItems[(int) transformType], transform).GetComponent<UltimateItem>();
                Cell cell = GetCell(item.CellPos);
                cell.AssignItem(newUltiItem);
                newUltiItem.CellPos = cell.position;
                newUltiItem.CompleteMove();
                ultiItems.Add(newUltiItem);
                allItems.Add(newUltiItem);
                newUltiItem.transform.DOPunchScale(Vector3.one / 4, .5f);

                TriggerItem(item);
                Destroy(item.gameObject);
                allItems.Remove(item);
            }
        }

        // disco balls blows up here
        vfxControl.PlayRemovalEffect(cellPos, Color.magenta);
        vfxControl.PlayRemovalEffect(cellPos, Color.magenta);
        RemoveItems(toRemove); // only the first one

        yield return new WaitForSeconds(1f); // or wait for items to fall

        // we sort ultis so they start blowing up from the top
        ultiItems.Sort();

        for(int i = 0; i < ultiItems.Count; i++)
        {
            // nooo
            //while(!ultiItems[i].DoneMoving)
            //    yield return null;

            yield return new WaitForSeconds(Random.Range(0f, .4f));

            if(ultiItems[i])
                TriggerItem(ultiItems[i], true);
        }

        if(onWaitDiscoCount == 0)
            RemoveItems(toRemove);

        onGoingRoutineCount--;
    }
    #endregion

    private void OnRoutinesEnded()
    {
        RemoveItems(toRemove);
        gameControl.InputEnabled = true;
    }

    private void TriggerItem(Item item, bool forceRemove = false, bool skipTimer = true)
    {
        // trigger once or depends on item?
        item.OnTrigger();
        vfxControl.PlayRemovalEffect(item.transform.position, item.FXColor);

        if(item is UltimateItem)
        {
            HandleUltiCast(item as UltimateItem, forceRemove);
        }
        else
        {
            // basic item or others
        }
    }

    private void HandleUltiCast(UltimateItem ultiItem, bool forceRemove = false)
    {
        switch(ultiItem.Type)
        {
            case UltimateType.RowRocket:
                StartCoroutine(RocketRoutine(ultiItem, Direction.Horizontal, forceRemove));
                break;
            case UltimateType.ColumnRocket:
                StartCoroutine(RocketRoutine(ultiItem, Direction.Vertical, forceRemove));
                break;
            case UltimateType.Bomb:
                StartCoroutine(BombRoutine(ultiItem, forceRemove));
                break;
            case UltimateType.Disco:
                StartCoroutine(DiscoRoutine(ultiItem as DiscoItem));
                break;
            case UltimateType.None:
                break;
            default:
                break;
        }
    }

    // takes in int[], what? wat is goin on here, wat wit da swic m8 ?¿
    private void HandleUltiMerge(int[] values, Vector2 position/*, int otherData*/, int markedID)
    {
        // this supports a maximum of 10 items and the resulting values must be known. there has to be a better way for extension
        if(values[0] > values[1])
            Extras.SwapInt(ref values[0], ref values[1]);

        //Debug.Log(values[0] + " : " + values[1]);
        
        // smaller one comes first, becoming the higher decimal
        switch(values[0] * 10 + values[1])
        {
            case 11:
                StartCoroutine(DualRocketRoutine(position));
                break;
            case 12:
                StartCoroutine(BombRocketRoutine(position));
                break;
            case 13:
                // oof
                StartCoroutine(AdvancedDiscoRoutine(position, markedID, values[0]));
                break;
            case 22:
                StartCoroutine(LargeBombRoutine(position, 3));
                break;
            case 23:
                // oof 2
                StartCoroutine(AdvancedDiscoRoutine(position, markedID, values[0]));
                break;
            case 33:
                StartCoroutine(LargeBombRoutine(position, Mathf.Max(size.x, size.y)));
                break;
        }
    }

    public IEnumerator TriggerChain(UltimateItem ultimateItem, float delay = 0)
    {
        WaitForSeconds whileDelay = new WaitForSeconds(.1f);
        yield return new WaitForSeconds(delay);

        TriggerItem(ultimateItem);

        while(onGoingRoutineCount > 0)
        {
            yield return whileDelay;
        }

        OnRoutinesEnded();
    }

    public HashSet<Item> FindAllItemsOfID(int ID)
    {
        HashSet<Item> itemsOfType = new HashSet<Item>();
        foreach(Item item in allItems)
        {
            if(item.ID == ID && !toRemove.Contains(item))
                itemsOfType.Add(item);
        }
        return itemsOfType;
    }

    // this is run each frame by GameControl after removing dots
    public void GridUpdateStep()
    {
        if(haltDuration > 0)
        {
            haltDuration -= Time.deltaTime;
            return;
        }

        // resets with each update but will reach 0 inside foreach when everything is in place
        int remainingDots = itemsToMove.Count;
        float movementStep = 0;
        // required to clear the list
        bool ended = false;

        foreach(Item item in itemsToMove)
        {
            if(item == null)
            {
                itemsToMove.Remove(item);
                break;
            }

            if(item.RemainingDistance <= 0)
            {
                remainingDots--;

                if(!item.DoneMoving)
                {
                    movedItems.Add(item);
                    if(item is BasicItem)
                        vfxControl.PlayLandEffect(item.CellPos);

                    float time = item.TimeSinceMove / 5;
                    if(time > .08f)
                    {
                        item.transform.DOPunchScale(new Vector3(.15f, 0, 0f), .2f, 10, .25f);
                        item.transform.DOLocalMoveY(item.transform.position.y + time, .25f).SetEase(bounceEase);
                    }
                    else
                    {
                        item.transform.DOPunchScale(new Vector3(.05f, 0, 0f), .25f, 10, .25f);
                        item.transform.DOPunchPosition(new Vector3(0, -.05f, 0f), .2f, 11);
                    }
                    //item.transform.DOPunchScale(new Vector3(.15f, -.15f, 0f), .25f, 10, .25f);
                    //item.transform.DOPunchPosition(new Vector3(0, -.15f, 0f), .2f, 11);

                    item.CompleteMove();
                    UpdateMergeMarkers();
                }

                // invoke done event to continue playing
                if(remainingDots == 0)
                {
                    GridUpdated.Invoke();
                    ended = true;
                    break;
                }
            }
            else
            {
                // move this dot this frame
                movementStep = Time.deltaTime * gravity * Mathf.Clamp(item.TimeSinceMove, 0, maxAccelerationDuration);
                item.MoveDownStep(movementStep);
            }
        }

        foreach(Item item in movedItems)
        {
            itemsToMove.Remove(item);
        }

        if(ended)
        {
            spawnCountAtColumn = new int[size.x];
            movedItems.Clear();
        }
    }

    // this part could be much better, try to keep the checked items, instead of clearing the list. Preventing unnecessary sprite changes and checks. 
    // However the sprite change may happen with a delay or even not happen if some blocks are not updated.

    // or maybe divide this even more, start a list when a movement is present then arrange the list to be checked according to that
    // something like "to be checked" might be a good idea instead of "all items"
    private HashSet<Item> checkedItems;
    private HashSet<Item> currentCheckItems;
    private void UpdateMergeMarkers()
    {
        // weird poliformizm, look into this 1
        checkedItems = new HashSet<Item>();

        int groupingCount = 0;

        foreach(Item item in allItems)
        {
            if(!checkedItems.Add(item)) continue; 

            // since the "recursive get connection" return only a single type, we can check for it once and have a safe foreach inside. still weird tho...
            if(item is BasicItem)
            {
                currentCheckItems = new HashSet<Item>();
                // weird poliformizim, look into this 2
                // switch depending on the item type
                groupingCount = GetBasicConnections(GetCell(item.CellPos), currentCheckItems);

                Sprite s = null; // item's own sprite or something?

                UltimateType ultimateType = ChooseType(groupingCount);
                if(ultimateType != UltimateType.None)
                    s = ultiMarkerSprites[(int) ultimateType];

                foreach(BasicItem basicGroupItem in currentCheckItems)
                {
                    basicGroupItem.SetMarkerSprite(s);
                }

                checkedItems.UnionWith(currentCheckItems);
            }
            else if(item is UltimateItem)
            {
                currentCheckItems = new HashSet<Item>();

                groupingCount = GetUltiConnections(GetCell(item.CellPos), currentCheckItems);

                foreach(UltimateItem ultiGroupItem in currentCheckItems)
                {
                    ultiGroupItem.ToggleShine((groupingCount > 1));
                }
            }
            else continue;
        }
    }

    private UltimateType ChooseType(int count)
    {
        if(count >= 9)
            return UltimateType.Disco;
        else if(count >= 7)
            return UltimateType.Bomb;
        else if(count >= 5)
            return Extras.GetProbability(.5f) ? UltimateType.RowRocket : UltimateType.ColumnRocket;
        else
            return UltimateType.None;
    }
}