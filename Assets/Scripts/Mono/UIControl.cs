﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    [Header("Messages")]
    public string titleMessage = "Two Gems";
    public string successMessage = "Good Job!";
    public string failedMessage = "Meh...";
    public string titleStartMessage = "PLAY";
    public string playAgainMessage = "PLAY AGAIN";

    [Header("In Game")]
    public Text redTargetText;
    public Text greenTargetText;
    public Text blueTargetText;
    public Text remainingMovesText;

    [Header("Other")]
    public Text mainMessageText;
    public Text startMessageText;
    public GameObject startPanel;
    public GameObject inGamePanel;
    public GameObject infoPanel;

    private void Start()
    {
        ActivateTitlePanel();
        ToggleInfoPanel(true);
    }

    public void ToggleInfoPanel(bool toggle)
    {
        infoPanel.SetActive(toggle);
    }

    public void ActivateTitlePanel()
    {
        inGamePanel.SetActive(false);

        startMessageText.text = titleMessage;
        startMessageText.text = titleStartMessage;

        startPanel.SetActive(true);
    }

    public void ActivatePostLevel(bool success)
    {
        inGamePanel.SetActive(false);

        startMessageText.text = playAgainMessage;
        if(success)
        {
            mainMessageText.text = successMessage;
        }
        else
        {
            mainMessageText.text = failedMessage;
        }
        startPanel.SetActive(true);
    }

    public void ActivateInGamePanel()
    {
        inGamePanel.SetActive(true);
        ToggleInfoPanel(false);
        startPanel.SetActive(false);
    }

    public void UpdateTargetText(int current, int target)
    {

    }

    public void UpdateMovesText(int current)
    {
        remainingMovesText.text = "<size=10>Moves</size>\n" + current;
    }
}
