﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UltimateItem : Item
{
    [Header("Ultimate Properties")]
    public UltimateType Type;
    public int MergeValue;

    protected ParticleSystem shineSystem;

    protected override void Awake()
    {
        base.Awake();
        shineSystem = GetComponentInChildren<ParticleSystem>();

        shineSystem.Stop();
    }

    public override void Merge(Vector2 mergePosition)
    {
        spriteRenderer.sortingOrder = 1;
        transform.DOMove(mergePosition, .35f).SetEase(Ease.InBack);
    }

    public override void OnTrigger()
    {
        gameObject.SetActive(false);
        // put fx here maybe?
    }

    public void ToggleShine(bool toggle)
    {
        if(toggle)
            shineSystem.Play();
        else
            shineSystem.Stop();
    }
}
