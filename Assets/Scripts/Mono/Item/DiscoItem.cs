﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DiscoItem : UltimateItem
{
    public int markedID;

    protected override void Awake()
    {
        base.Awake();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void UpdateSprite(Sprite s)
    {
        spriteRenderer.sprite = s;
    }

    public override void OnTrigger()
    {
        ToggleShine(true);
        MoveToFront(2);
        transform.DOShakePosition(.5f,.1f);
    }
}
