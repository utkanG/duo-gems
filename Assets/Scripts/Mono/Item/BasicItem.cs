﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BasicItem : Item
{
    private SpriteRenderer markerRenderer;

    protected override void Awake()
    {
        base.Awake();
        markerRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
    }

    public void SetMarkerSprite(Sprite newSprite)
    {
        markerRenderer.sprite = newSprite;
    }

    public override void OnTrigger()
    {
        gameObject.SetActive(false);
        // put fx here maybe?
    }

    public override void Merge(Vector2 mergePosition)
    {
        spriteRenderer.sortingOrder = 1;
        transform.DOMove(mergePosition, .35f).SetEase(Ease.InBack);
    }

    // Link Gameplay Stuff
    public bool CanConnectTo(Item other)
    {
        if(other.ID != ID) return false;

        float distance = Vector2.Distance(transform.position, other.transform.position);

        if(.1f < distance && distance < 1.1f)
            return true;
        else
            return false;
    }
}
