﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour, IEquatable<Item>, IComparable<Item>
{
    // make these a scriptable data
    public int ID;
    public Color FXColor;
    // make these a scriptable data

    protected SpriteRenderer spriteRenderer;

    [Header("Status Properties")]
    public float TimeSinceMove = 0; // hmm
    public float RemainingDistance = 0;
    public bool DoneMoving = false;
    public Vector2 CellPos;

    //private ItemData data;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void MoveDownStep(float moveStep)
    {
        transform.position += Vector3.down * moveStep;
        RemainingDistance -= moveStep;
        TimeSinceMove += Time.deltaTime;
    }

    public void CompleteMove()
    {
        transform.position = CellPos;
        DoneMoving = true;
        RemainingDistance = 0;
        TimeSinceMove = 0;
    }

    public virtual void MoveToFront(int sortingOrder)
    {
        spriteRenderer.sortingOrder = sortingOrder;
    }

    // these are mostly effect based
    public abstract void Merge(Vector2 targetPosition);

    // this is purely for things like animators and additional stuff, the functionality is planned to be done on the board via enums and stuff
    public abstract void OnTrigger();

    public bool Equals(Item other)
    {
        return other.GetHashCode() == GetHashCode();
    }

    public int CompareTo(Item other)
    {
        return (CellPos.y < other.CellPos.y) ? 1 : -1;
    }
}
