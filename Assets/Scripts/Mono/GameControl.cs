﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour
{
    public LevelData currentLevelData;

    public GameState state;
    public bool InputEnabled;

    [Header("References")]
    public UIControl uiControl;
    public VFXControl vfxControl;

    private Board grid;
    private Camera mainCamera;

    private HashSet<Item> selectedItems;
    private int selectedDotCount;

    private int remainingMoves;
    private int[] currentAmountOfTypes;
    private int[] targetAmountOfTypes;

    private void Awake()
    {
        mainCamera = Camera.main;
        grid = FindObjectOfType<Board>();
    }

    private void Start()
    {
        InputEnabled = true;
    }

    private void OnEnable()
    {
        grid.GridUpdated.AddListener(OnGridUpdated);
    }

    private void OnDisable()
    {
        grid.GridUpdated.RemoveListener(OnGridUpdated);
    }

    private void Update()
    {
        Vector2 touchPos = mainCamera.ScreenToWorldPoint(Input.mousePosition);

        if(Input.GetMouseButtonUp(0))
            BlastUnderFinger(touchPos);

        grid.GridUpdateStep();
    }

    private void BlastUnderFinger(Vector2 touchPos)
    {
        if(!InputEnabled) return;

        if(grid.IsInsideGrid(touchPos))
        {
            Cell cellUnderFinger = grid.GetCell(touchPos);

            //state = GameState.UpdatingGrid;

            // get item type here first, then decide on what to do depending on the count

            if(cellUnderFinger.Item is BasicItem)
            {
                int selectedItemCount = grid.GetBasicConnections(cellUnderFinger, selectedItems);

                if(selectedItemCount >= 5)
                {
                    InputEnabled = false;
                    StartCoroutine(grid.MergeBasicItems(selectedItems, cellUnderFinger));
                }
                else
                {
                    grid.BlastBasicItems(selectedItems);
                }
            }
            else if(cellUnderFinger.Item is UltimateItem)
            {
                int selectedItemCount = grid.GetUltiConnections(cellUnderFinger, selectedItems);

                if(selectedItemCount == 1)
                {
                    InputEnabled = false;
                    StartCoroutine(grid.TriggerChain(cellUnderFinger.Item as UltimateItem));
                }
                else if(selectedItemCount >= 2)
                {
                    InputEnabled = false;
                    StartCoroutine(grid.MergeUltiItems(selectedItems, cellUnderFinger));
                }
            }

            selectedItems.Clear();
            selectedDotCount = 0;
        }
    }

    // used by start button
    [ContextMenu("Start Game")]
    public void StartGame()
    {
        StopAllCoroutines();

        grid.GenerateGrid(currentLevelData);
        selectedItems = new HashSet<Item>();

        mainCamera.transform.position = new Vector3((float)(currentLevelData.Size.x)/2 - .5f, 8f, -10);

        //remainingMoves = currentLevelData.availableMoves;
        //currentAmountOfTypes = new int[System.Enum.GetNames(typeof(DotType)).Length];
        //targetAmountOfTypes = new int[System.Enum.GetNames(typeof(DotType)).Length];

        uiControl.ActivateInGamePanel();
        uiControl.UpdateMovesText(remainingMoves);
    }

    // used by reset button
    public void ResetGame()
    {
        StopAllCoroutines();

        grid.GenerateGrid(currentLevelData);
        selectedItems = new HashSet<Item>();

        mainCamera.transform.position = new Vector3((float) (currentLevelData.Size.x) / 2 - .5f, 8f, -10);

        //remainingMoves = currentLevelData.availableMoves;
        //currentAmountOfTypes = new int[System.Enum.GetNames(typeof(DotType)).Length];
        //targetAmountOfTypes = new int[System.Enum.GetNames(typeof(DotType)).Length];

        uiControl.ActivateInGamePanel();
        uiControl.UpdateMovesText(remainingMoves);
    }

    private void OnGridUpdated()
    {
        switch(CheckWinCondition())
        {
            //fail
            case -1:
                //state = GameState.Idle;
                uiControl.ActivatePostLevel(false);
                break;
            //win
            case 1:
                //state = GameState.Idle;
                uiControl.ActivatePostLevel(true);
                break;
            //nothing
            default:
                //state = GameState.Ready;
                break;
        }
    }

    // -1 fail, 0 continue, 1 success
    private int CheckWinCondition()
    {
        return 0;

        for(int i = 0; i < targetAmountOfTypes.Length; i++)
        {
            if(currentAmountOfTypes[i] >= targetAmountOfTypes[i]) // target reached
            {
                if(i == targetAmountOfTypes.Length - 1) // if all targets reached
                {
                    return 1;
                }
            }
            else // if not check moves
                break;
        }

        if(remainingMoves <= 0)
            return -1;
        else
            return 0;
    }

    // Link Gameplay Stuff
    //private void UpdateLineRenderer()
    //{
    //    connectionTrail.positionCount = Mathf.Clamp(selectedDotCount + 1, 2, 10000);
    //    for(int i = 0; i < selectedDotCount; i++)
    //    {
    //        connectionTrail.SetPosition(i, selectedDots[i].transform.position);
    //    }
    //}
    //private void LinkBegin(Vector2 touchPos)
    //{
    //    if(grid.IsInsideGrid(touchPos))
    //    {
    //        AbstractItem firstDot = grid.GetItem(touchPos);
    //        if(!firstDot) return;

    //        state = GameState.Dragging;

    //        selectedDots.Add(firstDot);
    //        selectedDotCount = 1;

    //        connectionTrail.positionCount = 1;
    //        connectionTrail.SetPosition(0, firstDot.transform.position);
    //        //connectionTrail.startColor = firstDot.GetColor();
    //        //connectionTrail.endColor = firstDot.GetColor();
    //        connectionTrail.gameObject.SetActive(true);

    //        //vfxControl.PlaySelectEffect(firstDot.transform.position, grid.GetColorFor(firstDot.type));

    //        // always has 2 points
    //        lastTrail.SetPosition(0, firstDot.transform.position);
    //        lastTrail.SetPosition(1, firstDot.transform.position);
    //        //lastTrail.startColor = firstDot.GetColor();
    //        //lastTrail.endColor = firstDot.GetColor();
    //        lastTrail.gameObject.SetActive(true);
    //    }
    //}

    //private void LinkHold(Vector2 touchPos)
    //{
    //    lastTrail.SetPosition(1, touchPos); // the point held by finger

    //    if(grid.IsInsideGrid(touchPos))
    //    {
    //        AbstractItem dotUnderFinger = grid.GetItem(touchPos);
    //        if(!dotUnderFinger) return;

    //        // add if can connect
    //        if(!selectedDots.Contains(dotUnderFinger))
    //        {
    //            if(selectedDots[selectedDotCount - 1].CanConnectTo(dotUnderFinger))
    //            {
    //                selectedDots.Add(dotUnderFinger);
    //                selectedDotCount++;

    //                connectionTrail.positionCount = selectedDotCount;
    //                connectionTrail.SetPosition(selectedDotCount - 1, dotUnderFinger.transform.position);

    //                lastTrail.SetPosition(0, dotUnderFinger.transform.position);

    //                //vfxControl.PlaySelectEffect(dotUnderFinger.transform.position, grid.GetColorFor(dotUnderFinger.type));
    //            }
    //        }

    //        // remove if went back
    //        if(selectedDotCount > 1)
    //        {
    //            if(selectedDots[selectedDotCount - 2] == dotUnderFinger)
    //            {
    //                selectedDotCount--;
    //                selectedDots.RemoveAt(selectedDotCount);

    //                connectionTrail.positionCount = selectedDotCount;
    //                connectionTrail.SetPosition(selectedDotCount - 1, dotUnderFinger.transform.position);
    //                lastTrail.SetPosition(0, dotUnderFinger.transform.position);
    //            }
    //        }
    //    }
    //}

    //private void LinkComplete()
    //{
    //    if(selectedDotCount > 1)
    //    {
    //        // add points toward target color
    //        //int typeValue = (int) selectedDots[0].type;
    //        //currentAmountOfTypes[typeValue] += selectedDotCount;
    //        //uiControl.UpdateTargetText(selectedDots[0].type, currentAmountOfTypes[typeValue], targetAmountOfTypes[typeValue]);

    //        // deduct a move
    //        remainingMoves--;
    //        uiControl.UpdateMovesText(remainingMoves);

    //        grid.UpdateItems(selectedDots);
    //        state = GameState.UpdatingGrid;
    //    }
    //    else
    //        state = GameState.Ready;

    //    selectedDots.Clear();
    //    selectedDotCount = 0;

    //    connectionTrail.positionCount = 0;
    //    connectionTrail.gameObject.SetActive(false);
    //    lastTrail.gameObject.SetActive(false);
    //}
}
