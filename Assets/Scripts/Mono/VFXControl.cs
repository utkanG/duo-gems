﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXControl : MonoBehaviour
{
    public ParticleSystem selectEffect;
    public ParticleSystem landEffect;
    public ParticleSystem dotRemoveEffect;
    public ParticleSystem ultiEffect;
    public ParticleSystem rocketEffect;

    private ParticleSystem.MainModule selectMain;
    private ParticleSystem.MainModule dotRemoveMain;

    private void Awake()
    {
        selectMain = selectEffect.main;
        dotRemoveMain = dotRemoveEffect.main;
    }

    public void PlaySelectEffect(Vector2 position, Color color)
    {
        selectEffect.transform.position = position;
        selectMain.startColor = new ParticleSystem.MinMaxGradient(color);
        selectEffect.Emit(1);
    }

    public void PlayUltiEffect(Vector2 position)
    {
        ultiEffect.transform.position = position;
        ultiEffect.Emit(1);
    }

    public void PlayRemovalEffect(Vector2 position, Color color)
    {
        dotRemoveEffect.transform.position = position;
        dotRemoveMain.startColor = new ParticleSystem.MinMaxGradient(color);
        dotRemoveEffect.Emit(1);
    }

    public void PlayLandEffect(Vector2 position)
    {
        landEffect.transform.position = position;
        landEffect.Emit(1);
    }

    public void PlayRocketEffect(Vector2 position, float rotation)
    {
        for(int i = 0; i < 2; i++)
        {
            rocketEffect.transform.rotation = Quaternion.Euler(i * 180 + rotation, 90, 0);
            rocketEffect.transform.position = position;
            rocketEffect.Emit(1);
        }
    }
}
